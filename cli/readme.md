Originally start with [cli](https://github.com/urfave/cli), but then, I found
cobra is more interesting since it's used by more popular OSS and its bash
completion looks easier to use.

## Caveat

For cobra:

- Bash completion libraries are different betwen bash 3.x (shipped with MacOS) and 4+.
- Zsh completion works better, but requires more manual works:
  - Gen the completion file, name it like `_<PROGRAM_NAME>`
  - Put the generated file somehwere in fpath
  - Put the program in `PATH`
  - Restart shell
