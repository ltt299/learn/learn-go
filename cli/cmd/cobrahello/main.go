package main

import (
	"fmt"
	"github.com/spf13/cobra"
	"log"
	"os"
)

func main() {
	cmds := []*cobra.Command{
		helloCmd,
		hiCmd,
		goodbyeCmd,
		completeCmd,
	}

	rootCmd.AddCommand(cmds...)

	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

var rootCmd = &cobra.Command{
	Use:   "cobrahello",
	Short: "hello world cobra application",
	Long: "An example of cobra application with some sub command that will " +
		"print some stupid dummy messages depends on the sub commands",
}

var helloCmd = &cobra.Command{
	Use:   "hello",
	Short: "Print hello world",
	Long:  "Print hello world message",
	Run: func(cmd *cobra.Command, args []string) {
		saySomething("Hello world", cmd, args)
	},
}

var hiCmd = &cobra.Command{
	Use:   "hi",
	Short: "Print hello world using hi prefix",
	Long:  "Print hello world message using hi prefix",
	Run: func(cmd *cobra.Command, args []string) {
		saySomething("Hi the world", cmd, args)
	},
}
var goodbyeCmd = &cobra.Command{
	Use:     "goodbye",
	Aliases: []string{"gb"},
	Short:   "Say goodbye to the world",
	Long:    "Say goodbye to the world...",
	Run: func(cmd *cobra.Command, args []string) {
		saySomething("Goodbye the world", cmd, args)
	},
}

var completeCmd = &cobra.Command{
	Use:   "complete",
	Short: "Generates bash completion scripts",
	Long: `To load completion run

. <(cobrahello complete)

To configure your bash shell to load completions for each session add to your bashrc

# ~/.bashrc or ~/.profile
. <(cobrahello complete)
`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) >= 1 && args[0] == "bash" {
			if err := rootCmd.GenBashCompletion(os.Stdout); err != nil {
				log.Fatal(err)
			}
			return
		}

		if err := rootCmd.GenZshCompletion(os.Stdout); err != nil {
			log.Fatal(err)
		}
	},
}

func saySomething(prefix string, cmd *cobra.Command, args []string) {
	fmt.Printf("[%v] %s\n", cmd.Name(), prefix)
	if len(args) == 0 {
		return
	}

	fmt.Println("And here is your args:")
	for i, x := range args {
		fmt.Printf("%d -> %v\n", i, x)
	}
}
