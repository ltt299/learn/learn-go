package main

import (
	"github.com/urfave/cli"
	"log"
	"os"
)

func main() {
	if err := cli.NewApp().Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
