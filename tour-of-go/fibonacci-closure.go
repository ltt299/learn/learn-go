package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
)

func fibonacci() func() int {
	a := 0
	b := 0
	return func() int {
		if b == 0 {
			b = 1
			return 0
		}

		a, b = b, a+b
		return a
	}
}

func main() {
	f := fibonacci()
	var end = 10
	if len(os.Args) > 1 {
		var err error
		end, err = strconv.Atoi(os.Args[1])
		if err != nil {
			log.Fatal(err)
		}
	}

	for i := 0; i < end; i++ {
		fmt.Println(f())
	}
}
