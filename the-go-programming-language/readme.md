Study code for The Go Programming Language 2nd
==============================================

The book is released in 2015, 3 years old already. Recent Go versions has many
architecture changes that **may** make some information in the book become obsolete.
However, it's the most praised book about Go, and is considered like like K&R
for C (Mr. Brian Kernighan) is one of the authors of the both books. Thus, this
will be my first book on Go.

I'll try to do as many exercises as possible, given limited time frame. Those
crazy exercises such as [ex1-24 in K&R for C](https://clc-wiki.net/wiki/K%26R2_solutions:Chapter_1:Exercise_24)
will be skipped.


