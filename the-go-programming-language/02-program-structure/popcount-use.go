package main

import (
	. "./popcount"
	"fmt"
)

func main() {
	fmt.Printf("%10s\t %20s\t %s\n", "Number", "Binary string", "Bit counts")
	for n := uint64(0); n < 100; n++ {
		pc := PopCount(n)
		fmt.Printf("%10d\t %20b\t %10d\n", n, n, pc)
	}
}
