package popcount

// This store the number of bit 1 in a given number i as index.
// such that:
// pc[0] = 0 because 0 has no bit turn on
// pc[2] = 1 because 2, in binary format ("10") has 1 bit turn on
var pc [265]byte

func init() {
	for i := range pc {
		// The number of bit 1 for the index i is equal to the number of bit of i/2, plus 1, if i%2 == 1
		pc[i] = pc[i/2] + byte(i&1)
	}
}

func PopCount(x uint64) int {
	return int(
		pc[byte(x>>(0*8))] +
			pc[byte(x>>(1*8))] +
			pc[byte(x>>(2*8))] +
			pc[byte(x>>(3*8))] +
			pc[byte(x>>(4*8))] +
			pc[byte(x>>(5*8))] +
			pc[byte(x>>(6*8))] +
			pc[byte(x>>(7*8))])
}
