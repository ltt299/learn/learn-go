package main

import "fmt"

func main() {
	naturals := make(chan int)
	squares := make(chan int)

	// Naturals
	go func() {
		for x := 0; x < 100; x++ {
			naturals <- x
		}
	}()

	// Squares
	go func() {
		for x := range naturals {
			squares <- x * x
		}
		close(squares)
	}()

	// Printer
	for y := range squares {
		fmt.Println(y)
	}
}
