package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
)

// Each client is stored as a channel of messages
type client struct {
	name string
	ch   chan string
}

func main() {
	listener, err := net.Listen("tcp", "localhost:8000")
	if err != nil {
		log.Fatal(err) // fatal is already contains os.Exit(1)
	}

	defer func() { listener.Close() }()
	// Map between client and its name
	clients := make(map[client]bool)

	// Announcing new client
	entering := make(chan client)

	// Announcing client leaving
	leaving := make(chan client)

	messages := make(chan string)

	go welcomeClient(entering, clients)
	go goodbyeClient(leaving, clients)
	go boardcast(messages, clients)

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println(err)
			continue // ignore the connection
		}
		go handleConn(conn, entering, messages, leaving)
	}
}

func handleConn(conn net.Conn, entering chan client, messages chan string, leaving chan client) {
	defer conn.Close()
	cli, err := createClient(conn)
	if err != nil {
		log.Printf("Closing connection %q due to error input\n", conn)
	} else {
		log.Printf("Client %v entering\n", cli)
		entering <- cli
		input := bufio.NewScanner(conn)
		go func() {
			for input.Scan() {
				if err := input.Err(); err != nil {
					log.Println(err)
					break
				}

				messages <- fmt.Sprintf("%s: %s\n", cli.name, input.Text())
			}

			leaving <- cli
		}()
		for msg := range cli.ch {
			fmt.Fprint(conn, msg)
		}
	}
}

func welcomeClient(ch <-chan client, clients map[client]bool) {
	for cli := range ch {
		for other := range clients {
			go func(other client) {
				other.ch <- fmt.Sprintf("%s  has joined\n", cli.name)
			}(other)
		}

		clients[cli] = true
		go func() { cli.ch <- "Welcome to the chat\n" }()
	}
}

func goodbyeClient(ch <-chan client, clients map[client]bool) {
	for cli := range ch {
		delete(clients, cli)
		for other := range clients {
			go func(other client) { other.ch <- fmt.Sprintf("%s has left\n", cli.name) }(other)
		}
	}
}

// Broadcast sends every new message to all available clients.
func boardcast(messages <-chan string, clients map[client]bool) {
	for msg := range messages {
		for cli := range clients {
			go func(cli client) { cli.ch <- msg }(cli)
		}
	}
}

// Get the client name and joining he to the chat, or close connection if
// thing is not ok
func createClient(conn net.Conn) (client, error) {
	fmt.Fprint(conn, "Please announce your name: ")
	scanner := bufio.NewScanner(conn)
	scanner.Scan()
	err := scanner.Err()
	if err != nil {
		return client{}, err
	}
	name := scanner.Text()

	// Each connection should have unique address,
	// no need for name collision check.
	who := fmt.Sprintf("%s (%s)", name, conn.RemoteAddr())
	return client{name: who, ch: make(chan string)}, nil
}
