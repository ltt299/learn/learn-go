package main

import (
	"fmt"
	"os"
	"time"
)

var done = make(chan struct{})

func cancelled() bool {
	select {
	case <-done:
		return true
	default:
		return false
	}
}

func main() {
	tick := time.Tick(1 * time.Second)

	go func() {
		os.Stdin.Read(make([]byte, 1))
		close(done)
	}()

	for c := 0; c < 10; c++ {
		fmt.Println(c)
		fmt.Println("current status:", cancelled())
		<-tick
	}
}
