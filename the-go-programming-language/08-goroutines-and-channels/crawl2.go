package main

import (
	"../05-functions/links"
	"fmt"
	"log"
	"os"
)

var tokens = make(chan struct{})

func main() {
	worklist := make(chan []string)
	// Start with command line arguments
	go func() {
		worklist <- os.Args[1:]
	}()

	// Crawl the web concurrently
	seen := make(map[string]bool)
	for list := range worklist {
		for _, url := range list {
			if !seen[url] {
				seen[url] = true
				go func(link string) { worklist <- crawl(link) }(url)
			}
		}
	}
}

func crawl(url string) []string {
	fmt.Println(url)

	tokens <- struct{}{} // acquire a token
	list, err := links.Extract(url)
	<-tokens // release the token

	if err != nil {
		log.Println(err)
	}

	return list
}
