// modified version of crawl1 to ensure that program will terminate once done
package main

import (
	"../05-functions/links"
	"fmt"
	"log"
	"os"
)

func main() {
	worklist := make(chan []string)

	// No need sync.WaitGroup here, because only one goroutine can read and write
	// to n: the main goroutine
	var n int // number of pending sends to worklist

	// Start with command line arguments
	n++
	go func() {
		worklist <- os.Args[1:]
	}()

	// Crawl the web concurrently
	seen := make(map[string]bool)
	for ; n > 0; n-- {
		list := <-worklist
		for _, url := range list {
			if !seen[url] {
				seen[url] = true
				n++
				go func(link string) { worklist <- crawl(link) }(url)
			}
		}
	}
}

func crawl(url string) []string {
	fmt.Println(url)
	list, err := links.Extract(url)
	if err != nil {
		log.Println(err)
	}
	return list
}
