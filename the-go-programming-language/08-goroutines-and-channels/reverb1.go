package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"strings"
	"time"
)

func main() {
	server, err := net.Listen("tcp", "localhost:8000")
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	for {
		conn, err := server.Accept()
		if err != nil {
			log.Fatal(err)
			continue
		}
		go handleConn(conn)
	}

}

func handleConn(c net.Conn) {
	input := bufio.NewScanner(c)
	for input.Scan() {
		go echo(c, input.Text(), 1*time.Second)
	}

	c.Close()
}

func echo(c net.Conn, s string, delay time.Duration) {
	fmt.Fprintf(c, "\t%s\n", strings.ToUpper(s))
	time.Sleep(delay)
	fmt.Fprintf(c, "\t%s\n", s)
	time.Sleep(delay)
	fmt.Fprintf(c, "\t%s\n", strings.ToLower(s))
}
