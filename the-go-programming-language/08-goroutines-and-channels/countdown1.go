package main

import (
	"fmt"
	"os"
	"time"
)

func main() {
	fmt.Println("Commencing countdown")
	// create abort channel
	abort := make(chan struct{})
	go func() {
		os.Stdin.Read(make([]byte, 1)) // read a single byte
		abort <- struct{}{}            // signal aborting
	}()

	tick := time.Tick(1 * time.Second)
	for c := 10; c > 0; c-- {
		select {
		case <-tick:
			fmt.Printf("%ds to launch\n", c)
		case <-abort:
			fmt.Println("Launch aborted")
			return
		}
	}
	launch()
}

func launch() {
	fmt.Println("Socket launched")
}
