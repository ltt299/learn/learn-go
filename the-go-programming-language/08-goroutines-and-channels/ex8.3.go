package main

import (
	"io"
	"log"
	"net"
	"os"
)

func main() {
	conn, err := net.Dial("tcp", "localhost:8000")
	if err != nil {
		log.Fatal(err)
	}

	done := make(chan struct{})
	go func() {
		// This copying won't stop until conn has nothing else
		io.Copy(os.Stdout, conn) // NOTE: ignoring errors
		log.Println("done")
		done <- struct{}{} // signal the main goroutine
	}()

	// This won't stop until Stdin is closed.
	mustCopy(conn, os.Stdin)
	log.Println("Done with copying Stdin")
	if cw, ok := conn.(*net.TCPConn); ok {
		cw.CloseWrite()
	} else {
		conn.Close()
	}

	<-done // wait for background goroutine to finish
}

func mustCopy(dst io.Writer, src io.Reader) {
	// This won't stop until everything from src is copied into dst
	if _, err := io.Copy(dst, src); err != nil {
		log.Fatal(err)
	}
}
