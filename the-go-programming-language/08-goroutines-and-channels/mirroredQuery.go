package main

import (
	"fmt"
	"net/http"
)

func main() {
	responses := make(chan string, 3)
	go func() { responses <- request("europe.gopl.io") }()
	go func() { responses <- request("americas.gopl.io") }()
	go func() { responses <- request("asia.gopl.io") }()

	fmt.Println(<-responses) // show the quickest response
}

func request(hostname string) string {
	_, _ = http.Get("https://" + hostname) // ignore error
	return hostname
}
