package main

import (
	"io"
	"log"
	"net"
	"os"
)

func main() {
	conn, err := net.Dial("tcp", "localhost:8000")
	if err != nil {
		log.Fatal(err)
	}

	done := make(chan struct{})
	go func() {
		// This copying won't stop until conn has nothing else
		io.Copy(os.Stdout, conn) // NOTE: ignoring errors
		log.Println("done")
		done <- struct{}{} // signal the main goroutine
	}()

	// This won't stop until Stdin is closed.
	mustCopy(conn, os.Stdin)

	conn.Close()
	<-done // wait for background goroutine to finish
}

func mustCopy(dst io.Writer, src io.Reader) {

	// This won't stop until everything from src is copied into dst
	if _, err := io.Copy(dst, src); err != nil {
		log.Fatal(err)
	}
}

// So, when Stdin is closed:
// - mustCopy will finish its work and stop.
// - conn will  have nothing else, just the copy at line 19 will done.
//		- channel "done" will signal that it's done.
// 			- Main goroutine will receive that signal, line 28 is unblocked
// 				- Nothing else to do, main goroutine will exit.
