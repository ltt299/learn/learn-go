package main

import (
	"fmt"
	. "unicode"
)

func main() {
	s := "      Something     wrong 				with \tthis \n  world."
	fmt.Println(s)
	fmt.Println(squashUnicodeSpaces(s))
}

func squashUnicodeSpaces(s string) string {
	ss := []rune(s)

	i, j := 0, 1
	for j < len(ss) {
		if IsSpace(ss[i]) && IsSpace(ss[j]) {
			j++
			continue
		}

		i++
		if IsSpace(ss[j]) {
			ss[i] = ' '
		} else {
			ss[i] = ss[j]
		}

		j++
	}

	ss = ss[:i+1]
	return string(ss)
}
