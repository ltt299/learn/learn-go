// This aims to test the statement in the book that array in Go is passed into function as a deep copy instead of
// pointer to memory. In other word, array modification within function don't affect the original array
package main

import "fmt"

func main() {
	var arr = [...]int{1, 2, 3}
	print(arr)
	print(double(arr))
	print(arr)
}

// This look funny! Because it only works for exactly array of 3 ints, to prove the point.
func double(arr [3]int) [3]int {
	for i, x := range arr {
		arr[i] = 2 * x
	}

	return arr
}

func print(arr [3]int) {
	fmt.Printf("%v\n", arr)
}
