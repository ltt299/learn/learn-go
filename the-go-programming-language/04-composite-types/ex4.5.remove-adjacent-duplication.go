package main

import (
	"fmt"
	"strings"
)

func main() {
	ss := strings.Split("zzzzzzzbuzz", "")
	fmt.Println(ss)
	ss = removeDuplicate(ss)
	fmt.Println(ss)
}

func removeDuplicate(ss []string) []string {
	if len(ss) < 2 { // nothing to do
		return ss
	}

	i, j := 0, 1
	for j < len(ss) {
		if ss[i] != ss[j] {
			i++
			ss[i] = ss[j]
		}
		j++
	}
	return ss[:i+1]
}
