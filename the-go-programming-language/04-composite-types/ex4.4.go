package main

import "fmt"

func main() {
	var data []int
	for i := 0; i < 10; i++ {
		data = append(data, i+1)
	}

	fmt.Println(data)
	data = rotate(data, 3)
	fmt.Println(data)
}
func rotate(arr []int, n int) []int {
	var m = len(arr)
	var c = make([]int, m)
	for i := 0; i < m; i++ {
		if i < n {
			c[m+i-n] = arr[i]
		} else {
			c[i-n] = arr[i]
		}
	}

	return c
}
