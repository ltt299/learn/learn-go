package main

import (
	"bufio"
	"crypto/sha256"
	"crypto/sha512"
	"flag"
	"fmt"
	"os"
)

type PrintHashFunc func(string)

var algorithm = flag.String("s", "sha256", "the algorithm to use, can be sha256, sha384, shd512.")

func main() {
	flag.Parse()
	switch *algorithm {
	case "sha256":
		hash(func(line string) {
			digest := sha256.Sum256([]byte(line))
			fmt.Printf("%x\n", digest)
		})

	case "sha384":
		hash(func(line string) {
			digest := sha512.Sum384([]byte(line))
			fmt.Printf("%x\n", digest)
		})
	case "sha512":
		hash(func(line string) {
			digest := sha512.Sum512([]byte(line))
			fmt.Printf("%x\n", digest)
		})
	default:
		fmt.Println("Invalid algorithm choice.")
	}
}

func hash(f PrintHashFunc) {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		f(scanner.Text())
	}
}
