package main

import "fmt"

func main() {
	var x, y []int
	for i := 0; i < 30; i++ {
		y = appendInt(x, i)
		fmt.Printf("%4d\t\tcap =%4d\t\tlen =%4d\t\t%v\n", i, cap(y), len(y), y)
		x = y
	}
}

func appendInt(x []int, y int) []int {
	var z []int
	zlen := len(x) + 1
	if zlen <= cap(x) {
		// There is room to grow. Extend the slice
		z = x[:zlen]
	} else {
		// There is insufficient space. Allocate a new array
		// Grow by doubling, for amortized linear complexity
		zcap := zlen
		if zcap < 2*len(x) {
			zcap = 2 * len(x)
		}

		z = make([]int, zlen, zcap)
		copy(z, x) // a built-in function; see text
	}

	z[len(x)] = y
	return z
}
