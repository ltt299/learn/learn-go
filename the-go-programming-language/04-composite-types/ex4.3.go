package main

import "fmt"

func main() {
	data := [10]int{}
	for i := 0; i < 10; i++ {
		data[i] = i + 1
	}

	fmt.Println(data)
	reverse(&data)
	fmt.Println(data)
}
func reverse(arr *[10]int) {
	for i, j := 0, len(*arr)-1; i < j; i, j = i+1, j-1 {
		arr[i], arr[j] = arr[j], arr[i]
	}
}
