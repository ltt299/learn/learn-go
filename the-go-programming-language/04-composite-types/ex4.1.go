package main

import (
	"crypto/sha256"
	"fmt"
)

var pc [256]int

func init() {
	for i := 0; i < 256; i++ {
		pc[i] = pc[i/2] + i%2
	}
}

func main() {
	c1 := sha256.Sum256([]byte("x"))
	c2 := sha256.Sum256([]byte("X"))

	fmt.Printf("%x\n", c1)
	fmt.Printf("%x\n", c2)
	fmt.Printf("Number of bit different: %d\n", diffBits(c1, c2))
}

func diffBits(a, b [32]byte) int {
	var c int
	for i := 0; i < 32; i++ {
		c += pc[a[i]^b[i]]
	}

	return c
}
