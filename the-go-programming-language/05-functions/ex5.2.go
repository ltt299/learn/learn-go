// Ex5.2 Populate HTML tags mapping with their occurrence in the HTML text
package main

import (
	"fmt"
	"golang.org/x/net/html"
	"os"
)

func main() {
	doc, err := html.Parse(os.Stdin)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}
	counts := make(map[string]int)
	countTags(counts, doc)
	for tag, count := range counts {
		fmt.Printf("%12s\t%3d\n", tag, count)
	}
}
func countTags(counts map[string]int, n *html.Node) {
	if n == nil {
		return
	}

	if n.Type == html.ElementNode {
		counts[n.Data]++
	}

	countTags(counts, n.FirstChild)
	countTags(counts, n.NextSibling)
}

/*

# Sample result
➜ ./bin/fetch https://golang.org  | go run 05-functions/ex5.2.go
           a     22
         pre      1
          br      3
        meta      3
      script     10
      button      1
      select      1
       title      2
        link      3
         div     34
        span      5
        form      1
       input      1
         svg      1
        path      2
      option      8
      iframe      1
        html      1
        head      1
        body      1
    textarea      2
*/
