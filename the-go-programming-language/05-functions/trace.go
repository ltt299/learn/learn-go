package main

import (
	"fmt"
	"time"
)

func main() {
	bigSlowOperation()
}

func bigSlowOperation() {
	defer trace("bigSlowOperation")() // don't forget the extra parentheses
	// .. lots of work...
	fmt.Println("Doing something in the middle")
	time.Sleep(2 * time.Second) // simulate slow operation by sleeping)
}

func trace(msg string) func() {
	start := time.Now()
	fmt.Printf("enter %s\n", msg)
	return func() {
		fmt.Printf("exit %s (%s)\n", msg, time.Since(start))
	}
}
