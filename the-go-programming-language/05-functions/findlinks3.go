package main

import (
	"fmt"
	"github.com/letientai299/learn-go/the-go-programming-language/05-functions/links"
	"log"
	"os"
)

// breadthFirst calls f for each item in the worklist
// Any items returned by f are added to the worklist.
// f is called at most once for each item.
func breadthFirst(f func(item string) []string, worklist []string) {
	seen := make(map[string]bool)
	for len(worklist) > 0 {
		items := worklist
		worklist = nil
		for _, item := range items {
			if !seen[item] {
				seen[item] = true
				worklist = append(worklist, f(item)...) // expand array into variadic
			}
		}
	}
}

func crawl(url string) []string {
	fmt.Println(url)
	list, err := links.Extract(url)
	if err != nil {
		log.Println(err)
	}

	return list
}

func main() {
	breadthFirst(crawl, os.Args[1:])
}
