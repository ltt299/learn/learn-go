package main

import (
	"fmt"
	"golang.org/x/net/html"
	"os"
	"strings"
)

func main() {
	doc, err := html.Parse(os.Stdin)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}

	showText(doc)
}
func showText(n *html.Node) {

	if n == nil {
		return
	}

	if n.Data == "script" || n.Data == "style" {
		return
	}

	if n.Type == html.TextNode {
		fmt.Println(strings.TrimSpace(n.Data))
	}

	showText(n.FirstChild)
	showText(n.NextSibling)

}
