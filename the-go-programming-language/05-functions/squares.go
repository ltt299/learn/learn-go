package main

import "fmt"

// squares returns a functions that returns the next square number each time
// it is called.
func squares() func() int {
	var x int
	return func() int {
		x++
		return x * x
	}
}

func main() {
	f := squares()
	for i := 0; i < 10; i++ {
		fmt.Println(f())
	}
}
