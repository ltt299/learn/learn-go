package main

import (
	"fmt"
	"golang.org/x/net/html"
)

//func main() {
//	res, err := http.Get(os.Args[1])
//	if err != nil {
//		fmt.Fprintf(os.Stderr, "%v\n", err)
//		os.Exit(1)
//	}
//
//	doc, err := html.Parse(res.Body)
//	res.Body.Close()
//	if err != nil {
//		fmt.Fprintf(os.Stderr, "%v\n", err)
//		os.Exit(1)
//	}
//
//	forEachNode(doc, startElement, endElement)
//}

// forEachNode calls the functions pre(x) and post(x) for each node x in the
// tree rooted at n. Both functions are optional.
// pre is called before the children are visisted (preorder) and
// post is called after (postorder)
func forEachNode(n *html.Node, pre, post func(n *html.Node)) {
	if pre != nil {
		pre(n)
	}

	for c := n.FirstChild; c != nil; c = c.NextSibling {
		forEachNode(c, pre, post)
	}

	if post != nil {
		post(n)
	}
}

var depth = 1

func startElement(n *html.Node) {
	if n.Type == html.ElementNode {
		fmt.Printf("%*s<%>\n", depth*2, "", n.Data)
		depth++
	}
}

func endElement(n *html.Node) {
	if n.Type == html.ElementNode {
		depth--
		fmt.Printf("%*s</%s>\n", depth*2, "", n.Data)
	}
}
