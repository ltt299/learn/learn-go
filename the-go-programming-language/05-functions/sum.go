package main

func sum(vals ...int) int {
	sum := 0
	for _, v := range vals {
		sum += v
	}

	return sum
}
