package main

import (
	"bytes"
	"fmt"
	"strings"
)

func main() {
	s :=
		`The chess pieces are identified by letters: K for king, Q for queen, 
R for rook, B for bishop, and N for knight. Describing a move uses the 
letter of the piece and the coordinates of its destination. A pair of 
these moves describes what happens in a single turn (with white going 
FIRST); FOR INSTANCE, THE NOTATION 2. NF3 NC6 INDICATES THAT WHITE MOVED 
A KNIGHT TO F3 AND BLACK MOVED A KNIGHT TO C6 ON THE SECOND TURN OF THE GAME.`

	fmt.Println("Original  string" + "\n--------------------------------")
	fmt.Println(s)
	fmt.Println("\n---------------")
	fmt.Println("Upper" + "\n--------------------------------")
	fmt.Println(expand(s, "chess", upper))
	fmt.Println("\n---------------")
	fmt.Println("Lower" + "\n--------------------------------")
	fmt.Println(expand(s, "IN", lower))
	fmt.Println("Lower 2" + "\n--------------------------------")
	fmt.Println(expand(s, "O", lower))
	fmt.Println("\n---------------")
}

func upper(s string) string {
	return strings.ToUpper(s)
}

func lower(s string) string {
	return strings.ToLower(s)
}

// Expand replaces any instance of "sub" it found in the input "s" with the
// result of f(sub).
// This is a more advanced function than the requirement in the book.
func expand(s, sub string, f func(string) string) string {
	replacement := f(sub)
	buf := new(bytes.Buffer)
	for i := strings.Index(s, sub); i != -1; i = strings.Index(s, sub) {
		buf.WriteString(s[0:i])
		buf.WriteString(replacement)
		s = s[i+len(sub):]
	}
	buf.WriteString(s)
	return buf.String()
}
