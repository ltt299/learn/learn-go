package main

import (
	"fmt"
)

func main() {
	fmt.Println(max(1))
	fmt.Println(max(1, 2, 3, 4, -10))
	fmt.Println(min(1))
	fmt.Println(min(1, 2, 3, 4, -10))
}

// max will returns the maximum value in the input values. This requires at
// least one input.
func max(v int, rest ...int) int {
	m := v
	for _, x := range rest {
		if m < x {
			m = x
		}
	}
	return m
}

// min will returns the minimum value in the input values. This requires at
// least one input.
func min(v int, rest ...int) int {
	m := v
	for _, x := range rest {
		if m > x {
			m = x
		}
	}
	return m
}
