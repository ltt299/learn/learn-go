package main

import (
	"bytes"
	"fmt"
	"strings"
)

func main() {
	fmt.Println(join(","))
	fmt.Println(join(",", "a"))
	fmt.Println(join(",", "a"))
	fmt.Println(join(",", strings.Split("abcdef", "")...))
	fmt.Println(join("-_-", strings.Split("abcdef", "")...))
}

func join(sep string, ss ...string) string {
	n := len(ss)
	if 0 == n {
		return ""
	}

	if n == 1 {
		return ss[0]
	}

	buf := new(bytes.Buffer)
	for _, s := range ss[:n-1] {
		buf.WriteString(s)
		buf.WriteString(sep)
	}

	buf.WriteString(ss[n-1])
	return buf.String()
}
