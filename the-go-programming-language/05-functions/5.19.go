package main

import "fmt"

func main() {
	fmt.Println(g(10))
}

func g(x int) (result int) {
	defer func() {
		recover()
		result = 2 * x
	}()
	panic("Always panic")
}
