package main

import (
	"fmt"
)

func main() {
	for i := 0; i < 10; i++ {
		double(i)
	}
}

func double(x int) (result int) {
	defer func() {
		fmt.Printf("double(%d) = %d\n", x, result)
	}()
	return x + x // This is ... neat, but also so dangerous
}
