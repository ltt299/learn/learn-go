package main

import (
	"fmt"
	"time"
)

func main() {
	var x, y int
	go func() {
		x = 1
		fmt.Printf("y = %d\n", y)
	}()
	go func() {
		y = 1
		fmt.Printf("x = %d\n", x)
	}()
	time.Sleep(1 * time.Second)
}
