package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(basename("a/b/c.go"))
	fmt.Println(basename("c.d.go"))
	fmt.Println(basename("abc"))
}

func basename(s string) string {
	start := strings.LastIndex(s, "/") // -1 if not found, thus, it safe to get start+1 as the starting index
	end := strings.LastIndex(s, ".")
	if end != -1 {
		return s[start+1 : end]
	}
	return s[start+1:]
}
