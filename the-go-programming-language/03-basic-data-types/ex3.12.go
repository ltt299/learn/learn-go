package main

import (
	"fmt"
	"sort"
	"strings"
)

func main() {
	fmt.Println(isAnagram("abc", "cba"))
	fmt.Println(isAnagram("abc1", "cba"))
	fmt.Println(isAnagram("abc1", "1111111111111111cba"))
	fmt.Println(isAnagram("abc12", "abc12"))
}

func isAnagram(a, b string) interface{} {
	as := strings.Split(a, "")
	bs := strings.Split(b, "")
	sort.Strings(as)
	sort.Strings(bs)
	if len(as) != len(bs) {
		return false
	}

	for i, _ := range as {
		if as[i] != bs[i] {
			return false
		}
	}

	return true
}
