package main

type Weekday int

const (
	Sunday Weekday = iota // so, instead of enum, Go use this special treatment to save memory?
	Monday
	Tuesday
	Wednesday
	Thursday
	Friday
	Saturday
)
