package main

import (
	"bytes"
	"fmt"
	"strings"
)

func main() {
	fmt.Println(comma("12321312313"))
	fmt.Println(comma("+12321312313"))
	fmt.Println(comma("-12321312313"))
	fmt.Println(comma(".0001"))
	fmt.Println(comma("1."))
	fmt.Println(comma("+12321312313.0001"))
	fmt.Println(comma("+12.0001"))
	fmt.Println(comma("12.0001"))
	fmt.Println(comma("-12321312313.0001"))
}

// This version of "comma" is for solving 3.10 and 3.11
// I have to admit that this look really ugly
func comma(s string) string {
	// split

	dotIndex := strings.Index(s, ".")
	var intPart string
	if dotIndex == -1 { // not a floating point numebr
		intPart = s
	} else {
		intPart = s[0:dotIndex]
	}

	if intPart == "" {
		return s
	}

	var buf bytes.Buffer

	// Write the sign if it is existed, and shorten the integer part accordingly
	if intPart[0] == '+' || intPart[0] == '-' {
		buf.WriteByte(intPart[0])
		intPart = intPart[1:]
	}

	// If the integer part is short enough, no need to do anything else
	n := len(intPart)
	if n <= 3 {
		return s
	}

	for i, j := n%3, 0; i < n-3; i, j = i+3, i {
		buf.Write([]byte(intPart[j:i]))
		buf.WriteByte(',')
	}

	if n >= 3 {
		buf.WriteString(intPart[n-3 : n])
	}

	if dotIndex != -1 {
		buf.WriteString(s[dotIndex:])
	}

	return buf.String()
}
