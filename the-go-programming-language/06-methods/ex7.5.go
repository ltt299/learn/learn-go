package main

import (
	"fmt"
	"io"
	"strings"
)

type LimitedReader struct {
	r     *io.Reader
	limit int
	count int
}

func (s *LimitedReader) Read(p []byte) (int, error) {
	n, err := (*s.r).Read(p)
	if err != nil {
		return n, err
	}

	if s.count+n <= s.limit {
		s.count += n
		return n, nil
	}

	return 0, io.EOF
}

func LimitReader(r io.Reader, n int) io.Reader {
	return &LimitedReader{r: &r, limit: n}
}

func main() {
	src := strings.NewReader("Try to do this exercises")
	limit := 5
	wrap := LimitReader(src, limit)
	fmt.Println(wrap.Read(make([]byte, 4)))
	fmt.Println(wrap.Read(make([]byte, 5)))
	fmt.Println(wrap.Read(make([]byte, 5)))
}
