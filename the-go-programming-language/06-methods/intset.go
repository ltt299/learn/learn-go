package main

import (
	"../02-program-structure/popcount"
	"bytes"
	"fmt"
)

// An IntSet is a set of small non-negative integers.
// Its zero value represents the empty set.
type IntSet struct {
	words []uint64
}

// Has reports whether the set contains the non-negative value x.
func (s *IntSet) Has(x int) bool {
	word, bit := x/64, uint(x%64)
	return word < len(s.words) && s.words[word]&(1<<bit) != 0
}

// Add adds the non negative value x to the set
func (s *IntSet) Add(x int) {
	word, bit := x/64, uint(x%64)
	for word >= len(s.words) {
		s.words = append(s.words, 0)
	}

	s.words[word] |= 1 << bit
}

// UnionWith sets s to the union of s and t
func (s *IntSet) UnionWith(t *IntSet) {
	for i, tword := range t.words {
		if i < len(s.words) {
			s.words[i] |= tword
		} else {
			s.words = append(s.words, tword)
		}
	}
}

func (s *IntSet) String() string {
	var buf bytes.Buffer
	buf.WriteByte('{')
	for i, word := range s.words {
		if word == 0 {
			continue
		}

		for j := 0; j < 64; j++ {
			if 0 != word&(1<<uint(j)) {
				if buf.Len() > len("{") {
					buf.WriteByte(' ')
				}
				fmt.Fprintf(&buf, "%d", 64*i+j)
			}
		}
	}

	buf.WriteByte('}')
	return buf.String()
}

// Len returns the number of elements
// ex6.1
func (s *IntSet) Len() int {
	total := 0
	for _, word := range s.words {
		if word == 0 {
			continue
		}

		total += popcount.PopCount(word)
	}
	return total
}

// Remove removes x from the set
// ex6.1
func (s *IntSet) Remove(x int) {
	word, bit := x/64, uint(x%64)
	if word > len(s.words) {
		return
	}

	s.words[word] &^= 1 << bit
}

// Clear removes all elements in the set
// ex6.1
func (s *IntSet) Clear() {
	s.words = nil
}

// Return a copy of the set
// ex6.1
func (s *IntSet) Copy() *IntSet {
	clone := &IntSet{words: make([]uint64, len(s.words))}
	copy(clone.words, s.words)
	return clone
}

// AddAll allows to add multiple values in one call
// ex6.2
func (s *IntSet) AddAll(vals ...int) {
	for _, v := range vals {
		s.Add(v)
	}
}

// Elems returns a slice contains all values stored in the set
// ex6.4
func (s *IntSet) Elems() []int {
	var result []int
	for i, v := range s.words {
		for j := 0; j < 64; j++ {
			if v&(1<<uint(j)) != 0 {
				result = append(result, 64*i+j)
			}
		}
	}

	return result
}

// demonstrate IntSet
func main() {
	var x, y IntSet
	x.Add(1)
	x.Add(144)
	x.Add(9)
	show(x)

	y.Add(9)
	y.Add(42)
	show(y)

	x.UnionWith(&y)
	show(x)

	x.Remove(9)
	show(x)

	x2 := *x.Copy()
	show(x2)

	x.Clear()
	show(x)
	show(x2) // x2 is not modified after x is cleared

	x.AddAll(1, 2, 3, 4, 5)
	show(x)

	fmt.Println(x.Elems())
}

func show(x IntSet) {
	fmt.Println(x.String(), ", Len =", x.Len())
}
