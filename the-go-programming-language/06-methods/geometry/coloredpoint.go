package geometry

import "image/color"

type ColoredPoint struct {
	Point // this one is embedded struct, without a concrete field name
	Color color.RGBA
}
