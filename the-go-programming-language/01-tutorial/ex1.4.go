package main

import (
	"bufio"
	"fmt"
	"os"
	"reflect"
)

type set map[string]bool

func main() {
	fmt.Println("Dup2 with filename")
	fmt.Println("----------------------------------------")
	counts := make(map[string]int)
	countFiles := make(map[string]set) // store file name with line
	files := os.Args[1:]
	if len(files) == 0 {
		countLines(os.Stdin, counts, countFiles)
	} else {
		for _, arg := range files {
			f, err := os.Open(arg)
			if err != nil {
				fmt.Fprintf(os.Stderr, "dup2: %v\n", err)
				continue
			}

			countLines(f, counts, countFiles)
			f.Close()
		}
	}

	fmt.Println("Count \t| Line \t| Files")
	fmt.Println("----- \t| ---- \t| -----")
	for line, n := range counts {
		files := reflect.ValueOf(countFiles[line]).MapKeys()
		fmt.Printf("%d \t| %s \t| %s\n", n, line, files)
	}
}

func countLines(f *os.File, counts map[string]int, countFiles map[string]set) {
	input := bufio.NewScanner(f)
	for input.Scan() {
		line := input.Text()
		counts[line]++

		if countFiles[line] == nil {
			countFiles[line] = make(set)
		}

		countFiles[line][f.Name()] = true
	}

	// NOTE: Ignoring potential errors from input.Err()
}
