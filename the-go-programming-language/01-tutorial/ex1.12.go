package main

import (
	"image"
	"image/color"
	"image/gif"
	"io"
	"log"
	"math"
	"math/rand"
	"net/http"
	"strconv"
)

var palette = []color.Color{
	color.Black,
	color.RGBA{G: 0xff, A: 0xff}, // green
	color.RGBA{R: 0xff, A: 0xff}, // red
	color.RGBA{B: 0xff, A: 0xff}, // blue
}

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe("localhost:8000", nil))
}

func handler(w http.ResponseWriter, r *http.Request) {
	const defaultCycles = 5.0
	if err := r.ParseForm(); err != nil {
		log.Print("Error during parsing form, use default config")
		lissajous(w, defaultCycles)
		return
	}

	input := r.FormValue("cycles")
	log.Printf("Cycle = %s", input)
	if cycle, err := strconv.Atoi(input); err != nil {
		log.Printf("Error during convert into from string (%s) to integer, use default config", input)
		lissajous(w, defaultCycles)
	} else {
		lissajous(w, float64(cycle))
	}
}

// copy code, since I don't know how to handle imports for duplicated functions
func lissajous(out io.Writer, cycles float64) {
	const (
		res     = 0.001 // angular resolution
		size    = 100   // image canvas covers [-size..+size]
		nFrames = 66    // number of animation frames
		delay   = 8     // delay between frames in 10ms units
	)

	freq := rand.Float64() * 3.0 // relative frequency of y oscillator
	anim := gif.GIF{LoopCount: nFrames}
	phase := 0.0 // phase difference
	for i := 0; i < nFrames; i++ {
		rect := image.Rect(0, 0, 2*size+1, 2*size+1)
		img := image.NewPaletted(rect, palette)
		for t := 0.0; t < cycles*2*math.Pi; t += res {
			x := math.Sin(t)
			y := math.Sin(t*freq + phase)
			img.SetColorIndex(size+int(x*size+0.5), size+int(y*size+0.5), uint8((i/8)%3+1))
		}

		phase += 0.1
		anim.Delay = append(anim.Delay, delay)
		anim.Image = append(anim.Image, img)
	}

	gif.EncodeAll(out, &anim) // NOTE: Ignoring encoding errors
}
