package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
)

func main() {
	for _, url := range os.Args[1:] {
		url := prependPrefix(url)
		res, err := http.Get(url)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%v\n", err)
			os.Exit(1)
		}

		// 1.9 print HTTP status code
		fmt.Printf("Status code: %s\n", res.Status)

		// 1.7 use io.Copy(dst, src)
		if _, e := io.Copy(os.Stdout, res.Body); e != nil {
			fmt.Fprintf(os.Stderr, "%v\n", err)
		}
	}
}

// 1.8
// check and prepend protocol prefix for url that miss that.
func prependPrefix(url string) string {
	if strings.HasPrefix(url, "http://") {
		return url
	}
	if strings.HasPrefix(url, "https://") {
		return url
	}

	return "http://" + url
}
