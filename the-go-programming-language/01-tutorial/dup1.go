// Dup1 prints the text of each line that appears more than once in the
// standard input, preceded by its count.
package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	counts := make(map[string]int) // counts is a map from string to int
	input := bufio.NewScanner(os.Stdin)
	for input.Scan() {
		counts[input.Text()]++
	}

	// NOTE: ignore potential errors from input.Err()
	for line, n := range counts { // here we use range the second times
		if n > 1 {
			fmt.Printf("%d\t%s\n", n, line)
		}
	}

}
