package main

import "testing"

func TestHI(t *testing.T) {
	name := "Golang"
	greeting := hi(name)
	expected := "Hello my friend, Golang"
	if greeting != expected {
		t.Errorf("The message is incorrect, got: %s, want: %s.", greeting, expected)
	}
}
