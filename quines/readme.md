[Quines](http://www.madore.org/~david/computers/quine.html) implemented in
golang.

Try it with:

```sh
go run quines.go > output.go && diff quines.go output.go
```

The expected result:

```
21c21
< var repeated = `
---
> var repeated = `
\ No newline at end of file
```

You see, the diff is the final newline, which is truncated on `quines.go` by
`gofmt`.

```
➜ l -la
Permissions Size User   Date Modified Git Name
drwxr-xr-x     - tai.le  9 Oct 21:42   -N .
drwxr-xr-x     - tai.le  9 Oct 21:39   -- ..
.rw-r--r--   266 tai.le  9 Oct 21:42   -N output.go
.rw-r--r--   267 tai.le  9 Oct 21:41   -N quines.go
.rw-r--r--   197 tai.le  9 Oct 21:40   -N readme.md
```

