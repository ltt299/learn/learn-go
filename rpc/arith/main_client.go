package main

import (
	"./core"
	"log"
	"net/rpc"
)

func main() {
	client, err := rpc.Dial("tcp", "localhost:1234")
	if err != nil {
		log.Fatal(err)
	}

	args := &core.Args{A: 10, B: 3}
	var mul int
	var div core.Quotient
	client.Call("Arith.Multiply", args, &mul)
	log.Printf("Mul: %v\n", mul)

	client.Call("Arith.Divide", args, &div)
	log.Printf("Div: %v\n", div)
}
