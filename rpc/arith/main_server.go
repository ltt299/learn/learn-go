package main

import (
	"./core"
	"log"
	"net"
	"net/rpc"
)

func main() {
	addr, err := net.ResolveTCPAddr("tcp", "localhost:1234")
	if err != nil {
		log.Fatal(err)
	}

	listener, err := net.ListenTCP("tcp", addr)
	if err != nil {
		log.Fatal(err)
	}

	rpc.Register(new(core.Arith))
	rpc.Accept(listener)
}
