package core

import (
	"github.com/pkg/errors"
	"log"
)

type Args struct {
	A, B int
}

type Quotient struct {
	Quo, Rem int
}

type Arith int

func (t *Arith) Multiply(args *Args, reply *int) error {
	log.Printf("Multiply: %v\n", args)
	*reply = args.A * args.B
	return nil
}

func (t *Arith) Divide(args *Args, reply *Quotient) error {
	log.Printf("Divide: %v\n", args)
	if args.B == 0 {
		return errors.New("divide by zero")
	}

	reply.Quo = args.A / args.B
	reply.Rem = args.A % args.B
	return nil
}
